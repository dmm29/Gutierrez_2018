# Clear the workspace
rm(list = ls())
graphics.off()

# set up libraries needed
library(Rsamtools)
library(GenomicRanges)
library(gplots)
library(viridis) #The viridis color palettes
library(RColorBrewer)
library(pastecs)


# get the bam file names
file_name = c( "nascent",
               "mature", 
               "bulk")

dir_path <- "~/your-directory-path/"

#define the nucleosomal read boudaries 
length_min = 140
length_max = 180


#chromosome coordinates 
options(scipen=999) 
chr_coordinates.df = data.frame(chr=(c("chrI", "chrII", "chrIII", "chrIV", "chrV", "chrVI", "chrVII", "chrVIII", "chrIX", "chrX", "chrXI", "chrXII",
                                       "chrXIII", "chrXIV", "chrXV","chrXVI")),
                                end=as.numeric(c("230218","813184", "316620", "1531933", "576874", "270161", "1090940", "562643", "439888", "745751", "666816", "1078177", "924431", "784333", "1091291", "948066")), stringsAsFactors=FALSE)

# create a list to store matrices 
data = vector("list")

# create lists for partial storage of nucleosome information 
chr_nuc_midAv_nascent=vector("list")
chr_nuc_midAv_mature=vector("list")
chr_nuc_midAv_bulk=vector("list")

a = vector("list")
b = vector("list")
c = vector("list")

#read in each chromosome information
for(r in 1:nrow(chr_coordinates.df)){
  chr = chr_coordinates.df[r,"chr"]        # chromosome name
  new_start= 1                             # chromosome start 
  new_end= chr_coordinates.df[r, "end"]    # chromosome end
  
  # create a GenomicRanges object with the information above to retrieve the read information from the bam file
  chr.gr = GRanges(seqnames= chr, ranges = IRanges(start =new_start , end = new_end ))
  
  p = ScanBamParam(what = c("rname", "strand", "pos", "isize"),which = chr.gr)
  
  #process the bam files 
  for (f in 1:3){
    
    #data files
    file_name.bam = paste(dir_path,file_name[f],".bam", sep='')
    file_name.bam.bai = paste(dir_path,file_name[f],".bam.bai",sep='')
    
    A_reads.l = scanBam(file = file_name.bam,
                        index = file_name.bam.bai,
                        param = p)
    
    #All the information from the range is in the first entry of the output_reads.l list
    # str(output_reads.l[[1]]) to see list structure
    
    #create a new GenomicRanges object for the reads from this list:
    A_reads.gr = GRanges(seqnames = A_reads.l[[1]]$rname,
                         ranges = IRanges(start = A_reads.l[[1]]$pos,
                                          width = A_reads.l[[1]]$isize))
    
    # extract reads that are of nucleosomal length previously defined.
    subset_data.gr = A_reads.gr[which(width(A_reads.gr) > length_min & width(A_reads.gr)< length_max)]
    
    #finding the mipoints of those reads 
    midpoints.gr =IRanges(start=mid(ranges(subset_data.gr)), width=1) 
    
    #convert GenomicRanges to a data frame 
    data[[f]] = as.data.frame(subset_data.gr)
    midpoints = as.data.frame(midpoints.gr)
    data[[f]]$mid=midpoints$start
    
      }
    
  }

  
  dm <- data[[3]]$mid
  
  #calculate turnpoints (nucleosome PEAKS)
  
  myDensity = density(dm, bw=30, kernel="gaussian", n=(new_end/5))
  
  tp = turnpoints(myDensity$y)
  
  #finding all of the nucleosome peaks within the chromosome 
  
  if (tp$firstispeak){
    d_peaks=myDensity$x[tp$tppos[seq(1,max(tp$tppos),by=2)]]
    d_peak_scores=myDensity$y[tp$tppos[seq(1,max(tp$tppos),by=2)]]
    print("first is a peak ")
  } else {
    d_peaks=myDensity$x[tp$tppos[seq(0,max(tp$tppos),by=2)]]
    d_peak_scores=myDensity$y[tp$tppos[seq(0,max(tp$tppos),by=2)]]
    print("first is NOT a peak ")
  }
  
  #keeping strong peaks 
  d_peak_scores[which(is.na(d_peak_scores))]=0
  d_peaks=d_peaks[which(d_peak_scores>1e-8)]
  
  #setting up matrices to store midpoint information for each nucleosome 
  a[[r]]= matrix(0,nrow=length(data[[1]]$mid), ncol=2)
  b[[r]]=matrix(0,nrow=length(data[[2]]$mid), ncol=2)
  c[[r]]=matrix(0,nrow=length(data[[3]]$mid), ncol=2)
  
  cat(paste("getting nascent","\n"))
  
  #column 1 is midpoint distance to dyad 
  #column 2 is the position of the midpoint 
  
  for(x in 1:length(data[[1]]$mid)){
    a[[r]][x,1]=min(abs(data[[1]]$mid[x]-d_peaks))
    a[[r]][x,2]=data[[1]]$mid[x]
  }
  
  cat(paste("getting mature","\n"))
  for(x in 1:length(data[[2]]$mid)){
    b[[r]][x,1]=min(abs(data[[2]]$mid[x]-d_peaks))
    b[[r]][x,2]=data[[2]]$mid[x]
  }
  
  cat(paste("getting bulk","\n"))
  for(x in 1:length(data[[3]]$mid)){
    c[[r]][x,1]=min(abs(data[[3]]$mid[x]-d_peaks))
    c[[r]][x,2]=data[[3]]$mid[x]
  }
  
  
  #setting up matrices to store nucleosome information 
  chr_nuc_midAv_nascent[[r]]= matrix(0,nrow=length(d_peaks), ncol=3)
  chr_nuc_midAv_mature[[r]]=matrix(0,nrow=length(d_peaks), ncol=3)
  chr_nuc_midAv_bulk[[r]]=matrix(0,nrow=length(d_peaks), ncol=3)
  
  
  # find the midpoints that overalp each nucleosome
  for(i in 1:length(d_peaks)){
    cat(paste("saving nucleosome", i,"on chromosome", r,"\n"))
    
    cstart = d_peaks[i] - 70
    cend = d_peaks[i] + 70
    
    #column 1 is the average midpoint distance to the nucleosome dyad
    #column 2 is the nucleosome dyad location 
    #column 3 is the total number of reads (occupancy)
    
    idx.a <- (which(a[[r]][,2] > cstart & a[[r]][,2] < cend))
    idxa = a[[r]][idx.a,] 
    if(length(idx.a) == 1){
      chr_nuc_midAv_nascent[[r]][i,1] <- (idxa[1])
      chr_nuc_midAv_nascent[[r]][i,2] <- d_peaks[i]
      chr_nuc_midAv_nascent[[r]][i,3] <- length(idx.a)
    } else{
      chr_nuc_midAv_nascent[[r]][i,1] <- mean(idxa[,1]) 
      chr_nuc_midAv_nascent[[r]][i,2] <- d_peaks[i]
      chr_nuc_midAv_nascent[[r]][i,3] <- nrow(idxa)
      
    }
    
    idx.b <- (which(b[[r]][,2] > cstart & b[[r]][,2] < cend))
    idxb = b[[r]][idx.b,]  
    if(length(idx.b) == 1){
      chr_nuc_midAv_mature[[r]][i,1] <- (idxb[1])
      chr_nuc_midAv_mature[[r]][i,2] <- d_peaks[i]
      chr_nuc_midAv_mature[[r]][i,3] <- length(idx.b)
    } else {
      chr_nuc_midAv_mature[[r]][i,1] <- mean(idxb[,1])  
      chr_nuc_midAv_mature[[r]][i,2] <- d_peaks[i]
      chr_nuc_midAv_mature[[r]][i,3] <- nrow(idxb)
      
    }
    
    idx <- (which(c[[r]][,2] > cstart & c[[r]][,2] < cend))
    idxc =c[[r]][idx,] 
    if(length(idx) == 1){
      chr_nuc_midAv_bulk[[r]][i,1] <- (idxc[1])
      chr_nuc_midAv_bulk[[r]][i,2] <- d_peaks[i]
      chr_nuc_midAv_bulk[[r]][i,3] <- length(idx)
    } else {
      chr_nuc_midAv_bulk[[r]][i,1] <- mean(idxc[,1])
      chr_nuc_midAv_bulk[[r]][i,2] <- d_peaks[i]
      chr_nuc_midAv_bulk[[r]][i,3] <- nrow(idxc)
      
    }
    
  }
  
}  


#MERGING ALL NUCLEOSOMES IN THE GENOME IN ONE DATA FRAME

chr_nuc_ratios.df = data.frame(
  Mreads = numeric(), 
  Nreads = numeric(), 
  Breads = numeric(),
  chr=character(),
  Mnuc_mdpAv = numeric(),
  Nnuc_mdpAv = numeric(),
  Bnuc_mdpAv = numeric(),
  nuc_peaks = numeric(),
  stringsAsFactors=FALSE
)


for(t in 1:16){
  chr_nuc_ratios1.df <- matrix(0,nrow=nrow(chr_nuc_midAv_bulk[[t]]), ncol=12)
  for(i in 1:nrow(chr_nuc_midAv_bulk[[t]])){
    cat(paste("saving nucleosome", i,"on chromosome", t,"\n"))
    chr_nuc_ratios1.df[i,4] =  chr_nuc_midAv_mature[[t]][i,3]
    chr_nuc_ratios1.df[i,5] =  chr_nuc_midAv_nascent[[t]][i,3]
    chr_nuc_ratios1.df[i,6] =  chr_nuc_midAv_bulk[[t]][i,3]
    chr_nuc_ratios1.df[i,7] =  chr_coordinates.df[t,1]
    chr_nuc_ratios1.df[i,9] =  chr_nuc_midAv_mature[[t]][i,1]
    chr_nuc_ratios1.df[i,10] =  chr_nuc_midAv_nascent[[t]][i,1]
    chr_nuc_ratios1.df[i,11] =  chr_nuc_midAv_bulk[[t]][i,1]
    chr_nuc_ratios1.df[i,12] = chr_nuc_midAv_bulk[[t]][i,2]
  }
  chr_nuc_ratios.df <- rbind(chr_nuc_ratios.df, chr_nuc_ratios1.df)
}

colnames(chr_nuc_ratios.df) <- c('Mreads','Nreads','Breads','chr','Mnuc_mdpAv',
                                 'Nnuc_mdpAv','Bnuc_mdpAv','nuc_peaks')




