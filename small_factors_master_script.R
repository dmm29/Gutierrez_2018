
library(Rsamtools)
library(GenomicRanges)
library(gplots)
library(pastecs)

#bam file names

file_name = c( "nascent",
               "mature", 
               "bulk")

dir_path <- "~/your-directory-path/"


#define the read boudaries (nucleosomal and/or subnucleosomal)
d_min = 10
d_max = 80

data.gr <- vector("list")

for(f in 1:2){
#data files
file_name.bam = paste(dir_path,file_name[f],".bam", sep='')
file_name.bam.bai = paste(dir_path,file_name[f],".bam.bai",sep='')

p = ScanBamParam(what = c("rname", "strand", "pos", "isize"))

A_reads.l = scanBam(file = file_name.bam,
                    index = file_name.bam.bai,
                    param = p)

#create a new GenomicRanges object for the reads
A_reads.gr = GRanges(seqnames = A_reads.l[[1]]$rname,
                     ranges = IRanges(start = A_reads.l[[1]]$pos,
                                      width = A_reads.l[[1]]$isize))

data.gr[[f]] = A_reads.gr[which(width(A_reads.gr) > d_min & width(A_reads.gr)< d_max)]
}
# sample reads of equal fragment length from the two bam files #
#obtain reads 
N <- as.data.frame(data.gr[[1]])
N <- N[-(which(N$seqnames == 'chrM')),]

M <- as.data.frame(data.gr[[2]])
M <- M[-(which(M$seqnames == 'chrM')),]

#create empty data frame and populate with reads
num <- data.frame(length = as.numeric(),
                  mature= as.numeric(),
                  nascent= as.numeric())

#find how many reads of each fragment size there are in each bam file 
for(i in 10:80){
  num[(i-9), 1] <- i
  num[(i-9), 2] <- length(which(M$width == i ))
  num[(i-9), 3] <- length(which(N$width == i ))
}

#sampling the same number of reads from mature as in nascent. 
Mss = data.frame(seqnames = factor(),
                 start= integer(),
                 end=integer(),
                 width=integer(),
                 strand=factor()
)

Nss = data.frame(seqnames = factor(),
                 start= integer(),
                 end=integer(),
                 width=integer(),
                 strand=factor()
)

for(i in 17:nrow(num)){
  
  idx <- which(M$width == num[i,1])
  idxn <- which(N$width == num[i,1])
  
  ss <- M[idx,]
  ssn <- N[idxn,]
  
  if(num[i,"mature"] > num[i,"nascent"]){
    
    ss1 <- ss[sample(nrow(ss), num[i,3]), ]
    Mss <- rbind(ss1, Mss)
    
    Nss <- rbind(ssn, Nss)
    
  }
  
  else{
    
    Mss <- rbind(ss, Mss)
    ss1n <- ssn[sample(nrow(ssn), num[i,2]), ]
    Nss <- rbind(ss1n, Nss)         
  } 
}

# finding the midpoint
Mss$mid <- as.numeric(Mss$start) + round(as.numeric(Mss$width)/2) 

Nss$mid <- as.numeric(Nss$start) + round(as.numeric(Nss$width)/2) 

# this is  to create a list of matrices to be able to call them individually later on.
data = vector("list")

chr_subnuc_midAv_nascent=vector("list")
chr_subnuc_midAv_mature=vector("list")

a = vector("list")
b = vector("list")

#setting up parameters
options(scipen=999) #disabling scientific notation 1= "230218",
chr_coordinates.df = data.frame(chr=(c("chrI", "chrII", "chrIII", "chrIV", "chrV", "chrVI", "chrVII", "chrVIII", "chrIX", "chrX", "chrXI", "chrXII",
                                       "chrXIII", "chrXIV", "chrXV","chrXVI")),
                                end=as.numeric(c("230218","813184", "316620", "1531933", "576874", "270161", "1090940", "562643", "439888", "745751", "666816", "1078177", "924431", "784333", "1091291", "948066")), stringsAsFactors=FALSE)
index_file[[1]] <- Nss
index_file[[2]] <- Mss


for(r in 1:nrow(chr_coordinates.df)){
  
  new_end = chr_coordinates.df$end[r]
  
  for(i in 1:2){
    idx = which(index_file[[i]]$seqnames == chr_coordinates.df$chr[r])
    data[[i]] = index_file[[i]][idx,]        
  }
  
  cat(paste("saving midpoints on chromosome", r,"\n"))
  
  #merging all three data sets to call all possible peaks in the data. 
  merged <- rbind(data[[1]], data[[2]])
  
  #calculate turnpoints PEAKS
  myDensity = density(merged$mid, bw=30, kernel="gaussian", n=(new_end/50))
  tp = turnpoints(myDensity$y)
  
  #finding all of the peaks within the range 
  
  if (tp$firstispeak){
    d_peaks=myDensity$x[tp$tppos[seq(1,max(tp$tppos),by=2)]]
    d_peak_scores=myDensity$y[tp$tppos[seq(1,max(tp$tppos),by=2)]]
    
    print("first is a peak ")
  } else {
    d_peaks=myDensity$x[tp$tppos[seq(0,max(tp$tppos),by=2)]]
    d_peak_scores=myDensity$y[tp$tppos[seq(0,max(tp$tppos),by=2)]]
    
    print("first is NOT a peak ")
    
  }
  
  d_peak_scores[which(is.na(d_peak_scores))]=0

  #minimum distance from a tp
  a[[r]]= matrix(0,nrow=length(data[[1]]$mid), ncol=2)
  b[[r]]=matrix(0,nrow=length(data[[2]]$mid), ncol=2)
  
  cat(paste("getting nascent","\n"))
  for(x in 1:length(data[[1]]$mid)){
        a[[r]][x,1]=data[[1]]$mid[x]
  }
  
  cat(paste("getting mature","\n"))
  for(x in 1:length(data[[2]]$mid)){
        b[[r]][x,1]=data[[2]]$mid[x]
  }
  
  chr_subnuc_midAv_nascent[[r]]= matrix(0,nrow=length(d_peaks), ncol=3)
  chr_subnuc_midAv_mature[[r]]=matrix(0,nrow=length(d_peaks), ncol=3)
  
  for(i in 1:length(d_peaks)){
    cat(paste("saving subnucleosome", i,"on chromosome", r,"\n"))
    
    cstart = d_peaks[i] - 15
    cend = d_peaks[i] + 15
    
    #column 1 is subnucleosomal peak position 
    #column 2 is 'total_midpoints' reads 
    
    idx.a <- (which(a[[r]][,2] > cstart & a[[r]][,2] < cend))
    idxa = a[[r]][idx.a,] 
    if(length(idx.a) == 1){
      chr_subnuc_midAv_nascent[[r]][i,1] <- d_peaks[i]
      chr_subnuc_midAv_nascent[[r]][i,2] <- length(idx.a)
    } else{
      chr_subnuc_midAv_nascent[[r]][i,1] <- d_peaks[i]
      chr_subnuc_midAv_nascent[[r]][i,2] <- nrow(idxa)
      
    }
    
    idx.b <- (which(b[[r]][,2] > cstart & b[[r]][,2] < cend))
    idxb = b[[r]][idx.b,]  
    if(length(idx.b) == 1){
      chr_subnuc_midAv_mature[[r]][i,1] <- d_peaks[i]
      chr_subnuc_midAv_mature[[r]][i,2] <- length(idx.b)
    } else { 
      chr_subnuc_midAv_mature[[r]][i,1] <- d_peaks[i]
      chr_subnuc_midAv_mature[[r]][i,2] <- nrow(idxb)
    }
  }
}

#setting up parameters
options(scipen=999) #disabling scientific notation 1= "230218",
chr_coordinates.df = data.frame(chr=(c("chrI", "chrII", "chrIII", "chrIV", "chrV", "chrVI", "chrVII", "chrVIII", "chrIX", "chrX", "chrXI", "chrXII",
                                       "chrXIII", "chrXIV", "chrXV","chrXVI")),
                                end=as.numeric(c("230218","813184", "316620", "1531933", "576874", "270161", "1090940", "562643", "439888", "745751", "666816", "1078177", "924431", "784333", "1091291", "948066")),
                                , stringsAsFactors=FALSE)

chr_subnuc_ratios.df = data.frame(
  Mreads = numeric(), 
  Nreads = numeric(), 
  chr=character(),
  subnuc_peaks = numeric(),
  stringsAsFactors=FALSE
)

#MERGING ALL factors IN THE GENOME IN ONE DATA FRAME

for(t in 1:16){
  chr_subnuc_ratios1.df <- matrix(0,nrow=nrow(chr_subnuc_midAv_mature[[t]]), ncol=7)
  for(i in 1:nrow(chr_subnuc_midAv_mature[[t]])){
    cat(paste("saving subnucleosome", i,"on chromosome", t,"\n"))
    chr_subnuc_ratios1.df[i,1] =  as.numeric(chr_subnuc_midAv_mature[[t]][i,2])
    chr_subnuc_ratios1.df[i,2] =  as.numeric(chr_subnuc_midAv_nascent[[t]][i,2])
    chr_subnuc_ratios1.df[i,3]= chr_coordinates.df[t,1]
    chr_subnuc_ratios1.df[i,4] = as.numeric(chr_subnuc_midAv_mature[[t]][i,1])
  }
  chr_subnuc_ratios.df <- rbind(chr_subnuc_ratios.df, chr_subnuc_ratios1.df)
}

colnames(chr_subnuc_ratios.df) <- c('Mreads' ,'Nreads','chr','subnuc_peaks')






