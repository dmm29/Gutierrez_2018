# Gutierrez_2018 
August 30, 2018

Contact Info:

David MacAlpine,
david.macalpine@duke.edu

Monica Gutierrez,
monica.gutierrez@duke.edu

General notes:

Figures and scripts for Gutierrez, 2019

"master" scripts generate the data frames that are the basis of all the analysis. To run the "master" scripts, the user needs to include the .bam files corresponding to "nascent", "mature", and (when necessary) "bulk" chromatin samples. Data frames required for processing and analysis include 1) the UCSC SacCer3 gene table, 2) the DNA replication origin data from Belsky, 2015, and 3) transcription factor data coordinates from MacIsaac, 2006. The directory "Figures_scripts" contains the subdirectories with processing and plotting scripts for each panel in the main figures.   