##### Samtools sequencing data processing ####
#/usr/local/bin/R

library(Rsamtools)
library(GenomicRanges)

##call functions
source('/figures/typhoon_plot_functions.R')

#bam file names
file_name = c("DM719_sacCer3_M1_2016-10-26-13-30", "DM526_sacCer3_M1_2016-10-24-19-45")

#individual plot title names, in same order as in the file_name
plot_title_1 = "EdU Labeled Chromatin"
plot_title_2 = "Bulk Chromatin"

#define the read boudaries (nucleosomal and/or subnucleosomal)
length_min = 20
length_max = 250

# set storage list for matrices (typhoon plot)
data = vector("list")

# Set the storage list  for nucleosome schematic
nuc_peaks.l = vector("list")

#setting up parameters
chr = "chrV" 

p=350625

new_start= p - 1000
new_end= p + 1050

#make matrix, a column for each fragment read length                       
matx.m = matrix(0, nrow=250, ncol=(new_end-new_start)+1)

chr.gr = GRanges(seqnames= chr, ranges = IRanges(start =new_start , end = new_end ))

p = ScanBamParam(what = c("rname", "strand", "pos", "isize"),which = chr.gr)

for (f in 1:2){
  
  file_name.bam = paste("/data/illumina_pipeline/aligned_experiments/",file_name[f],"/",file_name[f],file_extension[f],".bam", sep='')
  file_name.bam.bai = paste("/data/illumina_pipeline/aligned_experiments/",file_name[f],"/",file_name[f],file_extension[f],".bam.bai",sep='')
  
  A_reads.l = scanBam(file = file_name.bam,
                      index = file_name.bam.bai,
                      param = p)
  
  #All the information from the range is in the first entry of the output_reads.l list 
  # str(output_reads.l[[1]]) to see list structure
  
  #create a new GenomicRanges object for the reads from this list:
  A_reads.gr = GRanges(seqnames = A_reads.l[[1]]$rname,
                       ranges = IRanges(start = A_reads.l[[1]]$pos,
                                        width = A_reads.l[[1]]$isize))
  
  #defyning bp coverage.
  mat.gr = GRanges(seqnames = chr, ranges = IRanges(start=new_start:new_end,width=1))
  
  for(i in 20:250){
    subset_data.gr = A_reads.gr[which(width(A_reads.gr)==i)]
    
    start(subset_data.gr) = start(subset_data.gr) + width(subset_data.gr)/4
    end(subset_data.gr) = end(subset_data.gr) - width(subset_data.gr)/4
    
    matx.m[i,]=countOverlaps(mat.gr, subset_data.gr) 
  }
  
  #this is a list of the matrices to plot
  data[[f]] <- matx.m
  
  # Get the feature density
  nuc.v = GetMNaseFeatureDensity(file_name.bam, chr, new_start, new_end, 170, 180, 40)
  
  # Find the nucleosome peaks
  nuc_peaks.l[[f]] = GetDensityPeaks(nuc.v)
  
}


cat("Creating the plot...\n")
library(gplots)

scr.m = matrix(c(0.05, 1, 0.91, 1, #left, right, bottom, top (x1, x2, y1, y2)
                 0.05, 1, 0.85, 0.90,
                 0.05, 1, 0.8, 0.85,
                 0.05, 1, 0.45, 0.8,
                 0.05, 1, 0.1, 0.45,
                 
                 0.05, 1, 0, 0.05,
                 0.04, 0.05, 0.1, 0.8),
               ncol = 4, byrow = T
)

my_plot.s = split.screen(scr.m)

zm = 15
mn <- mean(nuc_peaks.l[[2]][,2])/mean(nuc_peaks.l[[1]][,2])
nuc_peaks.l[[1]][,2] <- nuc_peaks.l[[1]][,2] * mn

screen(my_plot.s[1])
par(mar = c(0,2.1,0,2.1), cex=0.8)  #margings order :bottom, left, top, right
make_gene_schematic(chr, new_start, new_end, proteinCoding = F,y_low = 0, y_high = 1.05)

screen(my_plot.s[2])
par(mar = c(0,2.1,0,2.1))
SetChromatinSchematic(x_start = new_start, x_end = new_end, y_start = 0, y_end = 1)
# Make the nucleosomes
PlotNucleosome(nuc_peaks.l[[1]])

screen(my_plot.s[3])
par(mar = c(0,2.1,0,2.1))
# Make the nucleosomes
SetChromatinSchematic(x_start = new_start, x_end = new_end, y_start = 0, y_end = 1)
PlotNucleosome(nuc_peaks.l[[2]])

screen(my_plot.s[4])
par(mar = c(1,2.1,1,2.1), cex=1, cex.main=1)
dens_dot_plot(data[[1]], z_min = 0, z_max = zm, plot_title=plot_title_1, x_axt = "n")
axis(side=1, at= seq(new_start, new_end, 500), labels=F, tck= -0.05)

screen(my_plot.s[5])
par(mar = c(1,2.1,1,2.1), cex=1, cex.main=1)
dens_dot_plot(data[[2]], z_min = 0, z_max = 5, plot_title=plot_title_2, x_axt = "n")
axis(side=1, at= seq(new_start, new_end, 500), labels= seq(new_start/1000, new_end/1000, 0.5), tck= -0.05)

screen(my_plot.s[6])
par(mar=c(0,0,0,0))
mtext(paste(chr,'Position (Kb)', sep = ' '), side = 1, cex=1)

screen(my_plot.s[7])
par(mar=c(0,0,0,0))
mtext('Fragment Length (bp)', side = 2, cex=1)

cat("\tComplete!\n")


