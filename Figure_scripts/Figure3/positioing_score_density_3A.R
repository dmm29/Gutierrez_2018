library(viridis) #The viridis color palettes

#runned the code on the command line from average_midpoint_distance_per_nucleosome_GW_m1__tpfixed_6_22_17_CURRENT_WORK_SPACE
chr_nuc_ratios.df <- read.csv("~/chr_nuc_ratios.csv", stringsAsFactors = F)

cl <- viridis(100, alpha = 1, begin = 0, end = 1, option = "D")
n=60
m=15

dens.m = matrix(c(0.05, 1, 0.1, 1,
                  0.05, 1, 0.08, 0.1, #xlab
                  0.02, 0.04, 0.1, 1), #ylab
                ncol = 4, byrow = T
)

dens_plot.s = split.screen(dens.m)

screen(dens_plot.s[1])
par(mar = c(2,2.1,2,2.1), cex=1) #bottom, left, top, right
options(repr.plot.width=5, repr.plot.height=5)

plot(density(chr_nuc_ratios.df$Bnuc_mdpAv, na.rm = T), col="grey", lwd="3", main=" ",
     xlab="Distance from Nucleosome Midpoint", xlim=c(0,60))
lines(density(chr_nuc_ratios.df$Mnuc_mdpAv, na.rm = T), col=cl[m], lwd="3")
lines(density(chr_nuc_ratios.df$Nnuc_mdpAv, na.rm = T), col=cl[n],lwd="3")
legend('topright', col="black", c("Nascent", "Mature", "Bulk"),
       pch=c(21,21, 21), pt.bg = c(cl[n], cl[m],"grey"),
       pt.cex=1.5,cex=1, 
       bty = "n", y.intersp=1.25, x.intersp=1)

screen(dens_plot.s[2])
par(mar=c(0,0,0,0))
mtext('Positioning Score', side = 1, cex=1)

screen(dens_plot.s[3])
par(mar=c(0,0,0,0))
mtext('Density Distribution', side = 2, cex=1)


