# Clear the workspace
graphics.off()
rm(list = ls())

# Load the functions
source('/data/home/mpg22/scripts/create_typhoon_plot_functions_sacCer3genetablemodified_xlimmodified.R')
source("/data/home/jab112/2014_mnase_manuscript/scripts/r_functions/mnase_acs_paper_final_figures.function.R")
source('/data/scripts/R-projects/TyphoonPlot/R/process_bam_functions.R')

# Set the dir
subpanel_dir = "/data/home/mpg22/Chromatin_Maturation_Manuscript_2018/Second_submission/revisions_figs/fig4_ablines/"

# Set up the plot

tiff(file ="/data/home/mpg22/Chromatin_Maturation_Manuscript_2018/Second_submission/revisions_figs/fig4_ablines/Gutierrez_Fig4.tiff",height = 10, width = 12, units = "in", res = 500, compression = "lzw")

# Setup the screen
overall_scr.m = matrix(c(0.03, 0.515, 0.55, 1, #left, right, top, bottom (x1, x2, y1, y2)
                         0.515, 1, 0.55, 1,
                         0.03, 0.515, 0.1, 0.5, 
                         0.515, 1, 0.1, 0.5,
                         
                         0.01, 0.04, 0.96, 1,
                         0.495, 0.525, 0.96, 1,
                         0.01, 0.04, 0.48, 0.51,
                         0.495, 0.525, 0.48, 0.51,
                         0, 1, 0, 0.1
),
ncol = 4, byrow = T
)

# Split the overall screen
close.screen(all.screens = T)

# Setup the screen
all_plots.s = split.screen(overall_scr.m)
par(oma = c(1, 1, 1, 1)) #bottom, left, top, right

# Panel A:
cat("\tMaking plot A\n")
screen(all_plots.s[1])
par(mai=c(0,0,0,0))
source(paste(subpanel_dir, "typhoon_plot_active_ori_fig4A.R", sep = ""))

# Panel B:
cat("\tMaking plot B\n")

screen(all_plots.s[2])
par(mai=c(0,0,0,0))
source(paste(subpanel_dir, "typhoon_plot_passive_ori_fig4B.R", sep = ""))

# Panel C:
cat("\tMaking plot C\n")
screen(all_plots.s[3])
par(mai=c(0,0,0,0))
source(paste(subpanel_dir, "active_passive_origins_boxplot_fig4C.R", sep = ""))

# Panel D:
cat("\tMaking plot D\n")
screen(all_plots.s[4])
par(mai=c(0,0,0,0))
source(paste(subpanel_dir,"density_active_passive_ORC_fig4D.R" , sep = ""))



screen(all_plots.s[5])
par(mai=c(0,0,0,0))
add_figure_label("A")

screen(all_plots.s[6])
par(mai=c(0,0,0,0))
add_figure_label("B")

screen(all_plots.s[7])
par(mai=c(0,0,0,0))
add_figure_label("C")

# Add in the Figure Labels
screen(all_plots.s[8])
par(mai=c(0,0,0,0))
add_figure_label("D")

# Add in the Figure Labels
screen(all_plots.s[9])
par(mai=c(0,0,0,0))
source(paste(subpanel_dir,"legend.R" , sep = ""))

mtext(expression(bold("Figure_6")), side =1, cex=1.5, outer=T, line = 0, adj = 0)

# Close all the screens
close.screen(all.screens = T)

# Close the device
dev.off()
